<?php

namespace App\Helpers;

class BingoHelper
{
    public static function fillNumbers(int $min, int $max): array
    {
        $i = $min;
        $numbers = [];
        do {
            $numbers[] = $i;
            $i++;
        } while ($i <= $max);
        return $numbers;
    }

    public static function callNumber(array $bingoNumbers, array $calledNumbers, array $range): array
    {
        $callableNumbers = [];
        foreach ($bingoNumbers as $key => $value) {
            if (!in_array($value, $calledNumbers)) {
                $callableNumbers[] = $value;
            }
        }
        shuffle($callableNumbers);
        
        $column = '';
        $calledNumber = $callableNumbers[0];
        
        foreach ($range as $key => $value){
            if($calledNumber >= $value['min'] && $calledNumber <= $value['max']){
                $column = $key;
            }
        }
        return [$column => $calledNumber];
    }

    public static function generateCard(array $bingoRange, int $lengthByColumn): array
    {
        $card = ['B' => [], 'I' => [], 'N' => [], 'G' => [], 'O' => [],];

        foreach ($bingoRange as $key => $value) {
            $card[$key] = BingoHelper::bingoCardFillColumn($value['min'], $value['max'], $lengthByColumn);
        }
        return $card;
    }

    public static function checkWinner(array $card, array $bingoNumbers): bool
    {
        $winner = true;
        $cardValidNumbers = [];
        foreach ($card as $key => $value){
            $cardValidNumbers = array_merge($cardValidNumbers, $value);
        }
        
        $bingoNumbersValidNumbers = [];
        foreach ($bingoNumbers as $key => $value){
            $bingoNumbersValidNumbers[] = array_values($value)[0];
        }
        
        foreach ($cardValidNumbers as $key => $value){
            if(!in_array($value, $bingoNumbersValidNumbers)){
                $winner = false;
                break;
            }
        }
        return $winner;
    }

    public static function bingoCardFillColumn(int $min, int $max, int $length): array
    {
        $inProcess = 0;
        $column = [];

        while ($inProcess == 0) {
            $randomNumber = BingoHelper::getRandomNumber($min, $max);
            if (!in_array($randomNumber, $column)) {
                $column[] = $randomNumber;
            }
            if (count($column) == 5) {
                $inProcess = 1;
            }
        }
        return $column;
    }

    public static function getRandomNumber(int $min, int $max): int
    {
        return random_int($min, $max);
    }

}