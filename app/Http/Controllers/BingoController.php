<?php

namespace App\Http\Controllers;

use App\Helpers\BingoHelper;
use Illuminate\Http\Request;

class BingoController extends Controller
{
    public $bingoNumbers = [];
    public $calledNumbers = [];
    public $bingoNumbersRange = [];
    public $clientCard = [];
    public $lengthByColumn = 5;
    
    public function __construct()
    {
        $this->bingoNumbers = BingoHelper::fillNumbers(1,75);
        $this->bingoNumbersRange = [
            'B' => ['min' => 1, 'max' => 15],
            'I' => ['min' => 16, 'max' => 30],
            'N' => ['min' => 31, 'max' => 45],
            'G' => ['min' => 46, 'max' => 60],
            'O' => ['min' => 61, 'max' => 75],
        ];
    }

    public function index(){
        $this->clientCard = BingoHelper::generateCard($this->bingoNumbersRange, $this->lengthByColumn);
        
        do{
            $callNumber = BingoHelper::callNumber($this->bingoNumbers, $this->calledNumbers, $this->bingoNumbersRange);
            $this->calledNumbers[] = $callNumber;
            echo "CALLED => ".array_keys($callNumber)[0]." ".array_values($callNumber)[0]."<br>";
        }while(count($this->calledNumbers) < 75);
        
        $isWinner = BingoHelper::checkWinner($this->clientCard, $this->calledNumbers);
        echo "====================";
        if($isWinner){
            echo "HAVE A WINNER";
        }else{
            echo "NEXT TIME MAYBE";
        }
    }
}
